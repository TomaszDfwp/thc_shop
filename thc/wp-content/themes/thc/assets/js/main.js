var my_console_log = function ($vardump, $name) {
	var $_name = $name || ' (name) ' + $vardump;
	console.log($vardump, $_name);
};


document.addEventListener("DOMContentLoaded", function (e) {
	if ( typeof jQuery === "function" ) {
		//MAIN MENU
		jQuery('.menu_header_mobile_open_button', document).on('click', function (e) {
			jQuery('.menu_header_mobile_section', document).addClass('active');
			jQuery('body', document).css({
				                             overflow : 'hidden'
			                             });
		});
		jQuery('.menu_header_mobile_close_button', document).on('click', function (e) {
			jQuery('.menu_header_mobile_section', document).removeClass('active');
			jQuery('body', document).css({
				                             overflow : 'auto'
			                             });
		});
		
		//SEARCH
		jQuery('.menu_shop_search_button', document).on('click', 'a', function (e) {
			e.preventDefault();
			var szukaj_header_outer_wrap = jQuery('.szukaj_header_outer_wrap', document);
			if ( szukaj_header_outer_wrap.hasClass('in_active') ) {
				jQuery('.szukaj_header_outer_wrap', document).removeClass('in_active');
			} else {
				jQuery('.szukaj_header_outer_wrap', document).addClass('in_active');
			}
		});
		
	}
});
