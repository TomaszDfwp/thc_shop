<?php
	
	// DEFINE PATHS
	define( 'ASSETS_URL', get_theme_file_uri( '/assets' ) );
	define( 'ASSETS_PATH', get_theme_file_path( '/assets' ) );
	define( 'CSS_ASSETS_URL', ASSETS_URL . '/css' );
	define( 'CSS_ASSETS_PATH', ASSETS_PATH . '/css' );
	define( 'JS_ASSETS_URL', ASSETS_URL . '/js' );
	define( 'JS_ASSETS_PATH', ASSETS_PATH . '/js' );
	
	// ACF_CUSTOM_FIELDS
	require_once( 'acf_custom_fields/acf-SIDEBAR/acf-SIDEBAR.php' );
	
	// Require CUSTOM_FUNCTIONS
	require_once( 'custom_functions/custom_functions.php' );
	
	add_action( 'after_setup_theme', '_custom_functions' );
	function _custom_functions() {
		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();
		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		/**
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'primary' => __( 'Header bottom menu', '_fwp' ),
		) );
	}
	
	add_action( 'wp_enqueue_scripts', 'register_assets_css_js' );
	function register_assets_css_js() {
		// CSS
		// wp_enqueue_style & wp_register_style (  $handle, $src = '', $deps = array(), $ver = false, $media = 'all' )
		$main_css = CSS_ASSETS_URL . '/main.min.css';
		wp_register_style( 'assets_main_css', $main_css, null, null );
		wp_enqueue_style( 'assets_main_css' );
		$current_css = CSS_ASSETS_URL . '/current.min.css';
		wp_register_style( 'assets_current_css', $current_css, null, null );
		wp_enqueue_style( 'assets_current_css' );
		$wp_css = get_stylesheet_uri();
		wp_register_style( 'assets_wp_css', $wp_css, null, null );
		wp_enqueue_style( 'assets_wp_css' );
		// JS
		// wp_register_script( $handle, $src, $deps = array(), $ver = false, $in_footer = false )
		$main_js = JS_ASSETS_URL . '/main.js';
		wp_register_script( 'assets_main_js', $main_js, array( 'jquery' ), null, true );
		wp_enqueue_script( 'assets_main_js' );
	}
	
	
	if ( function_exists( 'acf_add_options_page' ) ) {
		$parent = acf_add_options_page( array(
			'page_title' => 'THEME OPTION',
			'menu_title' => 'THEME OPTION',
			'menu_slug'  => 'theme_option',
			'redirect'   => true,
		) );
		acf_add_options_sub_page( array(
			'page_title'  => 'HEADER OPTION',
			'menu_title'  => 'HEADER OPTION',
			'parent_slug' => $parent[ 'menu_slug' ],
			'menu_slag'   => 'header_option',
		) );
		acf_add_options_sub_page( array(
			'page_title'  => 'FOOTER OPTION',
			'menu_title'  => 'FOOTER OPTION',
			'parent_slug' => $parent[ 'menu_slug' ],
			'menu_slag'   => 'footer_option',
		) );
		acf_add_options_sub_page( array(
			'page_title'  => 'STYLE OPTION',
			'menu_title'  => 'STYLE OPTION',
			'parent_slug' => $parent[ 'menu_slug' ],
			'menu_slag'   => 'style_option',
		) );
		acf_add_options_sub_page( array(
			'page_title'  => 'THEME OPTION',
			'menu_title'  => 'THEME OPTION',
			'parent_slug' => $parent[ 'menu_slug' ],
			'menu_slag'   => 'theme_option',
		) );
		
	}
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* todo ADD_THEME_OPTIONS
	 * or create New Option Page
	 * https://codex.wordpress.org/Creating_Options_Pages
* */
	
	global $current_theme_options;
	$current_theme_options = mb_strtoupper( __( "Theme Options", 'default' ) . ' - ' . get_option( 'current_theme' ) );
	
	// UPLOAD ENGINE
	// https://stackoverflow.com/questions/17668899/how-to-add-the-media-uploader-in-wordpress-plugin
	add_action( 'admin_enqueue_scripts', 'load_wp_media_files' );
	function load_wp_media_files() {
		wp_enqueue_media();
	}
	
	// ADD_THEME_OPTIONS
	add_action( 'admin_menu', 'add_theme_options' );
	function add_theme_options() {
		global $current_theme_options;
		add_options_page( $current_theme_options, $current_theme_options, 'manage_options', 'theme_options', 'theme_options' );
		//call register settings function
		add_action( 'admin_init', 'register_theme_options' );
	}
	
	function register_theme_options() {
		//register our settings
		register_setting( 'registered_theme_options_group', 'register_side_bar' );
		register_setting( 'registered_theme_options_group', 'google_maps_api_key' );
		register_setting( 'registered_theme_options_group', 'marker_mapy' );
		register_setting( 'registered_theme_options_group', 'kod_html_js' );
		register_setting( 'registered_theme_options_group', 'footer_bg_image' );
	}
	
	function theme_options() {
		global $current_theme_options; ?>
		<div class="wrap">
			<h2><?php echo $current_theme_options; ?></h2>
			<form method="post" action="options.php">
				
				<?php settings_fields( 'registered_theme_options_group' ); ?>
				<?php do_settings_sections( 'registered_theme_options_group' ); ?>

				<br>
				<hr>

				<h1>REGISTER SIDEBAR</h1>
				<div>
					<label for="register_side_bar">
						<strong>Wpisz nazwy widżetów, rozdzielając je przecinkami.</strong>
					</label>
					<br>
					<input type="text" id="register_side_bar" name="register_side_bar" size="65" value="<?php echo esc_attr( get_option( 'register_side_bar' ) ); ?>" />
					<div>
						<?php
							$registered_theme_side_bar = get_registered_theme_side_bar();
							if ( $registered_theme_side_bar != null ) {
								$_tmp_registered = '<h4>Reistered Theme Widgets List:</h4> ';
								foreach ( $registered_theme_side_bar as $item ) {
									$sidebar_name    = $item[ 'sidebar_name' ];
									$sidebar_slug    = $item[ 'sidebar_slug' ];
									$_tmp_registered .= '<div>Widget name: "' . $sidebar_name . '" (id: ' . $sidebar_slug . ')</div>';
								}
								echo $_tmp_registered;
							}
						?>
					</div>
				</div>

				<br>
				<hr>

				<h1>MAPA</h1>
				<h3>Google Maps API KEY</h3>
				<div>
					<label for="google_maps_api_key">
						<strong>API KEY:</strong>
					</label>
					<br>
					<input type="text" id="google_maps_api_key" name="google_maps_api_key" size="65" value="<?php echo esc_attr( get_option( 'google_maps_api_key' ) ); ?>" />
				</div>
				<br>
				<h3>Marker Mapy</h3>
				<div>
					<label for="marker_mapy"> <strong>(required size: 64px x 64px)</strong> </label>
					<br><br>
					<img id="img_marker_mapy" src="<?php echo wp_get_attachment_image_url( get_option( 'marker_mapy' ), 'large' ); // thumbnail medium large full medium_large thumbnail_map ; ?>" alt="" style="display: inline-block; margin: 0 20px; width: 64px; height: 64px; vertical-align: middle;">
					<input type="button" data-upload_input_id="#marker_mapy" data-upload_src="#img_marker_mapy" class="" value="<?php echo __( 'Wybierz Obrazek' ); ?>">
					<input type="hidden" id="marker_mapy" name="marker_mapy" size="100" value="<?php echo esc_attr( get_option( 'marker_mapy' ) ); ?>" />
					<br><br>
				</div>


				<br>
				<hr>


				<h1>Kod HTML</h1>
				<h3>CSS/JS/HTML</h3>
				<div>
					<label for="kod_html_js">
						<strong>Kod CSS/JS/HTML:</strong>
					</label>
					<br>
					<textarea name="kod_html_js" id="kod_html_js" cols="50" rows="10"><?php echo esc_attr( get_option( 'kod_html_js' ) ); ?></textarea>
				</div>


				<br>
				<hr>
				
				<?php submit_button(); ?>
			</form>

			<script type="text/javascript">
				jQuery(document).ready(function ($) {
					if ( 1 === 1 ) {
						$('[data-upload_input_id]').click(function (e) {
							e.preventDefault();
							$(this).data('upload_input_id');
							var input_id = $($(this).data('upload_input_id'));
							var image_src_id = $($(this).data('upload_src'));
							var image = wp.media({
								                     title : 'Upload Image',
								                     // mutiple: true if you want to upload multiple files at once
								                     multiple : false
							                     }).open().on('select', function (e) {
								// This will return the selected image from the Media Uploader, the result is an object
								var uploaded_image = image.state().get('selection').first();
//                                console.log(image.state().get('selection').toJSON());
								// We convert uploaded_image to a JSON object to make accessing it easier
								// Output to the console uploaded_image
//                                console.log(uploaded_image);
//                                console.log(uploaded_image.toJSON());
								var image_url = uploaded_image.toJSON().url;
								var image_id = uploaded_image.toJSON().id;
								// Let's assign the url value to the input field
								input_id.prop('value', image_id);
								image_src_id.prop('src', image_url);
							});
						});
					}
				});
			</script>


		</div>
	<?php }
	
	// add links/menus to the admin bar
	add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
	function mytheme_admin_bar_render() {
		global $wp_admin_bar;
		global $current_theme_options;
		$wp_admin_bar->add_menu( array(
			'parent' => false, // use 'false' for a root menu, or pass the ID of the parent menu
			'id'     => 'current_theme_options', // link ID, defaults to a sanitized title value
			'title'  => $current_theme_options, // link title
			'href'   => admin_url( 'options-general.php?page=theme_options' ), // name of file
			'meta'   => false // array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );
		) );
	}
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	* todo ADD_THEME_OPTIONS --END--
	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
	
	
	/*
	 * widget
	 * */
	
	function get_registered_theme_side_bar() {
		if ( get_option( 'register_side_bar' ) !== '' ) {
			$register_side_bar = explode( ',', trim( get_option( 'register_side_bar' ) ) );
			if ( is_array( $register_side_bar ) && count( $register_side_bar ) > 0 ) {
				$_tmp_side_bars = array();
				foreach ( $register_side_bar as $sidebar_name ) {
					$sidebar_name = trim( $sidebar_name );
					$sidebar_slug = preg_replace( '/-/', '_', sanitize_title_with_dashes( $sidebar_name ) );
					if ( ! empty( $sidebar_name ) ) {
						$_tmp_side_bars[] = array(
							'sidebar_name' => $sidebar_name,
							'sidebar_slug' => $sidebar_slug,
						);
					}
				}
				
				return $_tmp_side_bars;
			}
			
		} else {
			return null;
		}
		
	}
	
	add_action( 'widgets_init', 'register_theme_side_bar' );
	function register_theme_side_bar() {
		$registered_theme_side_bar = get_registered_theme_side_bar();
		if ( $registered_theme_side_bar != null ) {
			foreach ( $registered_theme_side_bar as $item ) {
				$sidebar_name = $item[ 'sidebar_name' ];
				$sidebar_slug = $item[ 'sidebar_slug' ];
				register_sidebar( array(
					'name'          => __( $sidebar_name, 'thc_theme' ),
					'id'            => $sidebar_slug,
					'description'   => __( 'Widżet ' . $sidebar_name, 'thc_theme' ),
					'before_widget' => '<div id="%1$s" class="widget %2$s">',
					'after_widget'  => '</div>',
					'before_title'  => '<h2 class="menu_sidebar_title ' . $sidebar_slug . '">',
					'after_title'   => '</h2>',
				) );
				
			}
		}
	}
