<?php /* Template Name: 404-PAGE */ ?>


<?php get_header(); ?>
<?php
	$id_404          = get_field( '404_page_id', 'option' );
	$naglowek        = get_field( 'naglowek', $id_404 );
	$opis_bledu      = get_field( 'opis_bledu', $id_404 );
	$tlo_strony      = get_field( 'tlo_strony', $id_404 );
	$naglowek_linkow = get_field( 'naglowek_linkow', $id_404 );
	$linki           = get_field( 'linki', $id_404 );
	
	$tlo_strony_url = '';
	$linki_return   = '';
	
	if ( $tlo_strony > 0 ) {
		$tlo_strony_url = wp_get_attachment_image_url( $tlo_strony, 'large' );
	}
	
	if ( is_array( $linki ) && count( $linki ) > 0 ) {
		$linki_return .= '<ul>';
		foreach ( $linki as $item ) {
			$title        = get_the_title( $item );
			$permalink    = get_permalink( $item );
			$linki_return .= '<li>';
			$linki_return .= '<a class="global_link color_red" href="' . $permalink . '">' . $title . '</a>';
			$linki_return .= '</li>';
		}
		$linki_return .= '</ul>';
	}
?>

	<section class="blad404_outer_wrap" style="background-image: url(<?php echo $tlo_strony_url; ?>)">
		<div class="blad404_wrap">
			<div class="page_wrap">

				<div class="blad404_naglowek">
					<?php echo $naglowek; ?>
				</div>
				<div class="blad404_opis_bledu_wrap">
					<div class="blad404_opis_bledu_bg_white">
						<?php echo $opis_bledu; ?>
					</div>
					<div class="blad404_opis_bledu_bg_green">
						<?php echo $opis_bledu; ?>
					</div>
					<div class="blad404_opis_bledu_text">
						<?php echo $opis_bledu; ?>
					</div>
				</div>
				<div class="blad404_naglowek_linkow_wrap">
					<div class="blad404_naglowek_linkow_bg_white">
						<?php echo $naglowek_linkow; ?>
					</div>
					<div class="blad404_naglowek_linkow_bg_green">
						<?php echo $naglowek_linkow; ?>
					</div>
					<div class="blad404_naglowek_linkow_text">
						<?php echo $naglowek_linkow; ?>
					</div>
				</div>
				<div class="blad404_linki">
					<?php echo $linki_return; ?>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>