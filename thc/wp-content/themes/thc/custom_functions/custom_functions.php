<?php
	
	function get_prawa_autorskie_footer() {
		$prawa_autorskie_footer        = get_field( 'prawa_autorskie_footer', 'option' );
		$prawa_autorskie_footer_return = '';
		if ( is_array( $prawa_autorskie_footer ) && count( $prawa_autorskie_footer ) > 0 ) {
			$prawa_autorskie_footer_return .= '<div class="prawa_autorskie_footer_wrap">';
			foreach ( $prawa_autorskie_footer as $item ) {
				$acf_fc_layout = $item[ 'acf_fc_layout' ];
				if ( $acf_fc_layout == 'tekst' ) {
					$tekst = $item[ 'tekst' ];
					if ( ! empty( $tekst ) ) {
						$prawa_autorskie_footer_return .= '<span class="prawa_autorskie_footer_item" ' . $acf_fc_layout . '>' . $tekst . '</span>';
					}
				} else if ( $acf_fc_layout == 'obraz' ) {
					$obraz = $item[ 'obraz' ];
					if ( ! empty( $obraz ) && $obraz > 0 ) {
						$obraz_url                     = wp_get_attachment_image_url( $obraz, 'thumbnail' );
						$obraz_img                     = "<img class='global_types_ikona' src='$obraz_url'>";
						$prawa_autorskie_footer_return .= '<span class="prawa_autorskie_footer_item ' . $acf_fc_layout . '">' . $obraz_img . '</span>';
					}
				}
			}
			$prawa_autorskie_footer_return .= '</div>';
			
		}
		
		return $prawa_autorskie_footer_return;
		
	}
	
	function get_ikony_footer() {
		$ikony_footer        = get_field( 'ikony_footer', 'option' );
		$ikony_footer_return = '';
		if ( is_array( $ikony_footer ) && count( $ikony_footer ) > 0 ) {
			$ikony_footer_return .= '<div class="ikony_footer_wrap">';
			foreach ( $ikony_footer as $item ) {
				$ikona = $item[ 'ikona' ];
				$link  = $item[ 'link' ];
				if ( ! empty( $ikona ) && $ikona > 0 ) {
					$ikona_url = wp_get_attachment_image_url( $ikona, 'thumbnail' );
					if ( $link == '' || $link == '#' ) {
						$ikony_footer_return .= "<span class='ikona_footer_link'><img class='ikona_footer_image' src='$ikona_url' alt=''></span>";
					} else {
						$ikony_footer_return .= "<a class='ikona_footer_link' href='$link'><img class='ikona_footer_image' src='$ikona_url' alt=''></a>";
					}
				}
			}
			$ikony_footer_return .= '</div>';
		}
		
		return $ikony_footer_return;
	}
	
	function get_menu_footer() {
		$menu_footer_id     = get_field( 'menu_footer_id', 'option' );
		$menu_footer_return = '';
		if ( ! empty( $menu_footer_id ) ) {
			$menu_footer_return .= wp_nav_menu(
				array(
					'depth'           => 1,
					'container'       => 'div',
					'container_class' => 'wp_nav_menu_wrap',
					'echo'            => false,
					'menu_class'      => 'menu_footer_wrap',
					'menu'            => $menu_footer_id,
					'menu_id'         => 'menu_footer',
				)
			);
		}
		
		return $menu_footer_return;
	}
	
	function get_logo_footer() {
		$home_url           = get_home_url();
		$logo_footer        = get_field( 'logo_footer', 'option' );
		$logo_footer_return = '';
		if ( ! empty( $logo_footer ) && $logo_footer > 0 ) {
			$logo_footer_url    = wp_get_attachment_image_url( $logo_footer, 'thumbnail' );
			$logo_footer_return .= '<div class="logo_footer_wrap">';
			$logo_footer_return .= "<a class='logo_footer_link' href='$home_url' ><img class='logo_footer_image' src='$logo_footer_url' alt=''></a>";
			$logo_footer_return .= '</div>';
		}
		
		return $logo_footer_return;
	}
	
	function get_logo_header() {
		$home_url           = get_home_url();
		$logo_header        = get_field( 'logo_header', 'option' );
		$logo_header_return = '';
		if ( ! empty( $logo_header ) && $logo_header > 0 ) {
			$logo_header_url    = wp_get_attachment_image_url( $logo_header, 'thumbnail' );
			$logo_header_return .= '<div class="logo_header_wrap">';
			$logo_header_return .= "<a class='logo_header_link' href='$home_url' ><img class='logo_header_image' src='$logo_header_url' alt=''></a>";
			$logo_header_return .= '</div>';
		}
		
		return $logo_header_return;
	}
	
	function get_dane_kontaktowe_footer() {
		$dane_kontaktowe_footer = get_field( 'dane_kontaktowe_footer', 'option' );
		$dane_kontaktowe_return = '';
		if ( is_array( $dane_kontaktowe_footer ) && count( $dane_kontaktowe_footer ) > 0 ) {
			
			foreach ( $dane_kontaktowe_footer as $global_types ) {
				
				$global_types = $global_types[ 'global_types' ];
				
				foreach ( $global_types as $item ) {
					$acf_fc_layout          = $item[ 'acf_fc_layout' ];
					$ikona                  = $item[ 'ikona' ];
					$link                   = $item[ 'link' ];
					$ikona_img              = '';
					$dane_kontaktowe_return .= "<div class='global_types_wrap'>";
					
					if ( ! empty( $ikona ) && $ikona > 0 ) {
						$ikona_url = wp_get_attachment_image_url( $ikona, 'thumbnail' );
						$ikona_img = "<img class='global_types_ikona' src='$ikona_url'>";
					}
					
					if ( $acf_fc_layout == 'telefon' ) {
						$dane_kontaktowe_return .= "<a class='global_types_telefon' href=\"tel:$link\">$ikona_img <span class='text'>$link</span></a>";
					} else if ( $acf_fc_layout == 'mail' ) {
						$dane_kontaktowe_return .= "<a class='global_types_mail' href=\"mailto:$link\">$ikona_img <span class='text'>$link</span></a>";
					} else if ( $acf_fc_layout == 'godziny_otwarcia' ) {
						$dane_kontaktowe_return .= "<span class='global_types_godziny_otwarcia'>$ikona_img <span class='text'>$link</span></span>";
					} else if ( $acf_fc_layout == 'tekst' ) {
						$dane_kontaktowe_return .= "<span class='global_types_tekst'>$ikona_img <span class='text'>$link</span></span>";
					}
					
					$dane_kontaktowe_return .= "</div>";
					
				}
				
			}
		}
		
		return $dane_kontaktowe_return;
	}
	
	function get_dane_kontaktowe_header() {
		$dane_kontaktowe_header = get_field( 'dane_kontaktowe_header', 'option' );
		$dane_kontaktowe_return = '';
		if ( is_array( $dane_kontaktowe_header ) && count( $dane_kontaktowe_header ) > 0 ) {
			
			foreach ( $dane_kontaktowe_header as $global_types ) {
				
				$global_types = $global_types[ 'global_types' ];
				
				foreach ( $global_types as $item ) {
					$acf_fc_layout          = $item[ 'acf_fc_layout' ];
					$ikona                  = $item[ 'ikona' ];
					$link                   = $item[ 'link' ];
					$ikona_img              = '';
					$dane_kontaktowe_return .= "<div class='global_types_wrap'>";
					
					if ( ! empty( $ikona ) && $ikona > 0 ) {
						$ikona_url = wp_get_attachment_image_url( $ikona, 'thumbnail' );
						$ikona_img = "<img class='global_types_ikona' src='$ikona_url'>";
					}
					
					if ( $acf_fc_layout == 'telefon' ) {
						$dane_kontaktowe_return .= "<a class='global_types_telefon' href=\"tel:$link\">$ikona_img <span class='text'>$link</span></a>";
					} else if ( $acf_fc_layout == 'mail' ) {
						$dane_kontaktowe_return .= "<a class='global_types_mail' href=\"mailto:$link\">$ikona_img <span class='text'>$link</span></a>";
					} else if ( $acf_fc_layout == 'godziny_otwarcia' ) {
						$dane_kontaktowe_return .= "<span class='global_types_godziny_otwarcia'>$ikona_img <span class='text'>$link</span></span>";
					} else if ( $acf_fc_layout == 'tekst' ) {
						$dane_kontaktowe_return .= "<span class='global_types_tekst'>$ikona_img <span class='text'>$link</span></span>";
					}
					
					$dane_kontaktowe_return .= "</div>";
					
				}
				
			}
		}
		
		return $dane_kontaktowe_return;
	}
	
	function get_szukaj_header() {
		$szukaj_header        = get_field( 'szukaj_header', 'option' );
		$ikona_szukaj         = $szukaj_header[ 'ikona_szukaj' ];
		$szukaj_placeholder   = $szukaj_header[ 'szukaj_placeholder' ];
		$szukaj_header_return = '';
		if ( ! empty( $ikona_szukaj ) && $ikona_szukaj > 0 ) {
			$ikona_szukaj_url     = wp_get_attachment_image_url( $ikona_szukaj, 'thumbnail' );
			$szukaj_header_return .= '<div class="szukaj_header_wrap">';
			$szukaj_header_return .= "<form action=''><div class='search_submit_wrap'><input name='search_input' placeholder='$szukaj_placeholder' type='text'> <button type='submit'> <img class='submit_image' src='$ikona_szukaj_url' alt=''></div></form></button>";
			$szukaj_header_return .= '</div>';
		}
		
		return $szukaj_header_return;
	}
	
	
	function get_menu_header() {
		$menu_header_id     = get_field( 'menu_header_id', 'option' );
		$menu_header_return = '';
		if ( ! empty( $menu_header_id ) ) {
			$menu_header_return .= wp_nav_menu(
				array(
					'depth'           => 1,
					'container'       => 'div',
					'container_class' => 'wp_nav_menu_wrap',
					'echo'            => false,
					'menu_class'      => 'menu_header_wrap',
					'menu'            => $menu_header_id,
					'menu_id'         => 'menu_header',
				)
			);
		}
		
		return $menu_header_return;
	}
	
	function get_menu_shop() {
		$menu_shop_id     = get_field( 'menu_shop_id', 'option' );
		$menu_shop_return = '';
		if ( ! empty( $menu_shop_id ) ) {
			$menu_shop_return .= wp_nav_menu(
				array(
					'depth'           => 1,
					'container'       => 'div',
					'container_class' => 'wp_nav_menu_wrap',
					'echo'            => false,
					'menu_class'      => 'menu_shop_wrap',
					'menu'            => $menu_shop_id,
					'menu_id'         => 'menu_shop',
				)
			);
		}
		
		return $menu_shop_return;
	}
	
	
	function sc_dump( $dump, $name = '' ) {
		/*LOG_OUTPUT*/
		if ( defined( 'DOING_AJAX' ) ) {
			return;
		}
		echo "<script>console.log( '----------############# CONSOLE.LOG DUMP @@@ JSON START @@@ #############----------' );</script>";
		$dump_type        = gettype( $dump );
		$dump_json_encode = json_encode( $dump );
		if ( $name !== '' ) {
			echo "<script>console.log( 'NAME : $name');</script>";
		}
		echo "<script>console.log( 'TYPE : $dump_type');</script>";
		echo "<script>console.log(JSON.stringify($dump_json_encode));</script>";
		echo "<script>console.log( '----------#############  END CONSOLE.LOG DUMP @@@ JSON END @@@ #############----------' );</script>";
		/*END_LOG_OUTPUT*/
	}