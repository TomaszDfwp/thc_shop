<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		
		<?php
		//WIDGET
		$wybrany_widget  = get_field( 'wybrany_widget', get_the_ID() );
		$pozycja_widgetu = get_field( 'pozycja_widgetu', get_the_ID() );
		if ( ! empty( $wybrany_widget ) ) {
			echo '<section class="bulider_with_widget_wrap">';
			echo '<section class="wybrany_widget_wrap ' . $pozycja_widgetu . '" >';
			dynamic_sidebar( $wybrany_widget );
			echo '</section>';
			echo '<section class="bulider_wrap">';
			get_template_part( 'bulider/bulider' );
			echo '</section>';
			echo '</section>';
		} else {
			echo '<section class="bulider_wrap">';
			get_template_part( 'bulider/bulider' );
			echo '</section>';
		}
		?>
	<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php get_footer(); ?>

