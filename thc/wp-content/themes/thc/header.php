<!doctype html>

<!--[if lt IE 7]>
<html class="ie6"><![endif]-->
<!--[if IE 7]>
<html class="ie7"><![endif]-->
<!--[if IE 8]>
<html class="ie8"><![endif]-->
<!--[if IE 9]>
<html class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
	
	<?php
		$title           = is_single() ? get_the_title() : wp_title( '|', false, 'right' );
		$description     = is_single() ? get_the_excerpt() : get_bloginfo( 'name' ) . ' - ' . get_bloginfo( 'description' );
		$thumb_id        = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'thumbnail-size', true );
		$thumb_url       = $thumb_url_array[ 0 ];
	?>

	<!-- General Meta START -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- General Meta END -->

	<!-- SEO Meta START -->
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="description" content="<?php echo $description; ?>" />
	<!-- SEO Meta END -->


	<!-- Social media tags -->
	<meta property="og:title" content="<?php echo $title; ?>">
	<meta property="og:description" content="<?php echo $description; ?>">
	<meta property="og:image" content="<?php
		if ( is_single() ) {
			echo $thumb_url;
		} else {
			echo $thumb_url;
		} /* !---- dodać obraz*/
	?>">
	<meta property="og:url" content="<?php
		if ( is_single() ) {
			echo get_permalink();
		} else {
			echo get_site_url();
		}
	?>">
	<meta name="twitter:card" content="summary_large_image">

	<!-- Wordpress Meta  -->
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


	<!-- Wordpress Head START -->
	<?php wp_head(); ?>
	<!-- Wordpress Head END -->

</head>

<body <?php body_class(); ?>>

<?php get_template_part( 'sections/section_header' ); ?>

