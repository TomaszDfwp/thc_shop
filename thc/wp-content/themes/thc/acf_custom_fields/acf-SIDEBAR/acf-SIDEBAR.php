<?php
	
	/*
	Plugin Name: Advanced Custom Fields: SIDEBAR
	Plugin URI: PLUGIN_URL
	Description: SHORT_DESCRIPTION
	Version: 1.0.0
	Author: AUTHOR_NAME
	Author URI: AUTHOR_URL
	License: GPLv2 or later
	License URI: http://www.gnu.org/licenses/gpl-2.0.html
	*/

// exit if accessed directly
	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}
// check if class already exists
	if ( ! class_exists( 'NAMESPACE_acf_plugin_SIDEBAR' ) ) :
		
		class NAMESPACE_acf_plugin_SIDEBAR {
			
			// vars
			var $settings;
			
			
			/*
			*  __construct
			*
			*  This function will setup the class functionality
			*
			*  @type	function
			*  @date	17/02/2016
			*  @since	1.0.0
			*
			*  @param	n/a
			*  @return	n/a
			*/
			
			function __construct() {
				
				// settings
				// - these will be passed into the field class.
				$this->settings = array(
					'version' => '1.0.0',
					'url'     => get_theme_file_uri(),
					'path'    => realpath( dirname( __FILE__ ) ),
				);
				
				// include field
				add_action( 'acf/include_field_types', array( $this, 'include_field_types' ) ); // v5
				add_action( 'acf/register_fields', array( $this, 'include_field_types' ) ); // v4
				
				
			}
			
			
			/*
			*  include_field_types
			*
			*  This function will include the field type class
			*
			*  @type	function
			*  @date	17/02/2016
			*  @since	1.0.0
			*
			*  @param	$version (int) major ACF version. Defaults to false
			*  @return	n/a
			*/
			
			function include_field_types() {
				// include
				include_once( realpath( dirname( __FILE__ ) ) . '/fields/class-NAMESPACE-acf-field-FIELD-NAME-v5.php' );
			}
			
		}


// initialize
		new NAMESPACE_acf_plugin_SIDEBAR();


// class_exists check
	endif;
