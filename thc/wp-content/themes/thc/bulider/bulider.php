<?php
	$_this_post_id   = get_the_ID();
	$_this_post_type = get_post_type( $_this_post_id );
	//BULIDER
	$bulider = get_field( 'bulider', $_this_post_id );

	
	
	if ( ! empty( $bulider ) ) {
		foreach ( $bulider as $bulider_array ) {
			$acf_fc_layout = $bulider_array[ 'acf_fc_layout' ];
			if ( $acf_fc_layout == 'wysiwyg' ) {
				include( locate_template( 'bulider/wysiwyg/wysiwyg.php' ) );
			}
			unset( $acf_fc_layout, $bulider_array );
			$acf_fc_layout = null;
			$bulider_array = null;
		}
	}