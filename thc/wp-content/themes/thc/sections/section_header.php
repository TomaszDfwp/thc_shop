<?php
	$kolory_header = get_field( 'kolory_header', 'option' );
	$kolor_tla     = $kolory_header[ 'kolor_tla' ];
	$kolor_tekstu  = $kolory_header[ 'kolor_tekstu' ];
?>

<section class="main_menu_outer_wrap" style="background-color: <?php echo $kolor_tla; ?>; color: <?php echo $kolor_tekstu; ?>;">
	<section class="page_wrap">

		<div class="menu_header_mobile_outer_wrap">
			<button class="menu_header_mobile_open_button">
				MENU
			</button>
			<div class="menu_header_mobile_section">
				<?php echo get_menu_header(); ?>
				<button class="menu_header_mobile_close_button">
					x
				</button>
			</div>
		</div>

		<div class="logo_header_outer_wrap">
			<?php echo get_logo_header(); ?>
		</div>

		<div class="menu_shop_mobile_outer_wrap">
			<?php echo get_menu_shop(); ?>
		</div>

		<div class="dane_kontaktowe_szukaj_outer_wrap">
			<div class="dane_kontaktowe_header_outer_wrap">
				<?php echo get_dane_kontaktowe_header(); ?>
			</div>
			<div class="szukaj_header_outer_wrap">
				<?php echo get_szukaj_header(); ?>
			</div>
		</div>
		<div class="main_menu_shop_outer_wrap">
			<div class="menu_header_outer_wrap">
				<?php echo get_menu_header(); ?>
			</div>
			<div class="menu_shop_outer_wrap">
				<?php echo get_menu_shop(); ?>
			</div>
		</div>
	</section>
</section>