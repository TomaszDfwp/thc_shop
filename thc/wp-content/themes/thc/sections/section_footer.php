<?php
	$kolory_footer       = get_field( 'kolory_footer', 'option' );
	$kolor_tla_footer    = $kolory_footer[ 'kolor_tla' ];
	$kolor_tekstu_footer = $kolory_footer[ 'kolor_tekstu' ];
?>

<section class="line_separator"></section>
<section class="section_footer_outer_wrap" style="background-color: <?php echo $kolor_tla_footer; ?>; color: <?php echo $kolor_tekstu_footer; ?>;">
	<section class="page_wrap">
		<div class="logo_dane_kontaktowe_footer_outer_wrap">
			<div class="logo_footer_outer_wrap">
				<?php echo get_logo_footer(); ?>
			</div>
			<div class="dane_kontaktowe_footer_outer_wrap">
				<?php echo get_dane_kontaktowe_footer(); ?>
			</div>
		</div>
	</section>
	<section class="line_separator"></section>
	<section class="page_wrap">
		<div class="menu_footer_copy_outer_wrap">
			<div class="menu_footer_outer_wrap">
				<?php echo get_menu_footer(); ?>
			</div>
			<div class="prawa_autorskie_footer_outer_wrap">
				<?php echo get_prawa_autorskie_footer(); ?>
			</div>
		</div>
	</section>
</section>
<section class="line_separator"></section>
<section class="ikony_footer_outer_wrap">
	<section class="page_wrap">
		<?php echo get_ikony_footer(); ?>
	</section>
</section>

