<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
		<div style="text-align: center;">
			<h1>
				<?php the_title(); ?>
			</h1>
			<div>
				<?php the_content(); ?>
			</div>
			<div>
				<?php the_post_thumbnail(); ?>
			</div>
		</div>
	<?php endwhile; ?>
<?php else : ?>
<?php endif; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>